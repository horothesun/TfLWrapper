enablePlugins(JavaAppPackaging)

scalaVersion := "2.13.0"

name := "tfl-wrapper"
organization := "com.horothesun"
version := "0.1"

val http4sVersion = "0.21.0-M3"
val tapirVersion = "0.9.1"

libraryDependencies ++= Seq(
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2",
  "dev.zio" %% "zio" % "1.0.0-RC11-1",
  "dev.zio" %% "zio-interop-cats" % "2.0.0.0-RC2",
  "com.softwaremill.tapir" %% "tapir-core" % tapirVersion,
  "com.softwaremill.tapir" %% "tapir-http4s-server" % tapirVersion,
  "com.softwaremill.tapir" %% "tapir-json-circe" % tapirVersion,
  "com.softwaremill.tapir" %% "tapir-openapi-docs" % tapirVersion,
  "com.softwaremill.tapir" %% "tapir-openapi-circe-yaml" % tapirVersion,
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-server" % http4sVersion,
  "org.http4s" %% "http4s-client" % http4sVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-blaze-client" % http4sVersion,
  "org.http4s" %% "http4s-circe" % http4sVersion,
  "io.circe" %% "circe-generic" % "0.12.0-RC1",
  "org.slf4j" % "slf4j-simple" % "1.7.26",
  "org.webjars" % "webjars-locator" % "0.36",
  "org.webjars" % "swagger-ui" % "3.23.4",
  "org.scalamock" %% "scalamock" % "4.3.0" % "test",
  "org.scalatest" %% "scalatest" % "3.0.8" % "test",
  "org.scalacheck" %% "scalacheck" % "1.14.0" % "test"
)
