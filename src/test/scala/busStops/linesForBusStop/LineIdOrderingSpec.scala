package busStops.linesForBusStop

import org.scalacheck._
import org.scalacheck.Prop._
import org.scalacheck.Prop.AnyOperators

object LineIdOrderingSpec extends Properties("LineIdOrdering"){

  final case class NumLineIds(numA: Int, numB: Int)
  final case class AlphaLineIds(alphaA: String, alphaB: String)
  final case class MixedLineIds(num: Int, alpha: String)

  private def numLineIdsGen(): Gen[NumLineIds] = for {
    numA <- Gen.posNum[Int]
    numB <- Gen.posNum[Int]
  } yield NumLineIds(numA, numB)

  private def alphaLineIdsGen(): Gen[AlphaLineIds] = for {
    alphaA <- Gen.alphaStr
    alphaB <- Gen.alphaStr
  } yield AlphaLineIds(alphaA, alphaB)

  private def mixedLineIdsGen(): Gen[MixedLineIds] = for {
    num   <- Gen.posNum[Int]
    alpha <- Gen.alphaStr
  } yield MixedLineIds(num, alpha)

  private implicit val numLineIdsArbitrary: Arbitrary[NumLineIds] = Arbitrary(numLineIdsGen())
  private implicit val alphaLineIdsArbitrary: Arbitrary[AlphaLineIds] = Arbitrary(alphaLineIdsGen())
  private implicit val mixedLineIdsArbitrary: Arbitrary[MixedLineIds] = Arbitrary(mixedLineIdsGen())

  property("comparing two numeric lineIds as numbers") = forAll { numLineIds: NumLineIds =>
    LineIdOrdering.compare(s"${numLineIds.numA}", s"${numLineIds.numB}") ?=
      Ordering[Int].compare(numLineIds.numA, numLineIds.numB)
  }

  property("comparing two alphabetic lineIds as strings") = forAll { alphaLineIds: AlphaLineIds =>
    LineIdOrdering.compare(alphaLineIds.alphaA, alphaLineIds.alphaB) ?=
      Ordering[String].compare(alphaLineIds.alphaA, alphaLineIds.alphaB)
  }

  property("comparing two mixed lineIds as numeric < alphabetic") = forAll { mixedLineIds: MixedLineIds =>
    LineIdOrdering.compare(s"${mixedLineIds.num}", mixedLineIds.alpha) < 0
    LineIdOrdering.compare(mixedLineIds.alpha, s"${mixedLineIds.num}") > 0
  }
}
