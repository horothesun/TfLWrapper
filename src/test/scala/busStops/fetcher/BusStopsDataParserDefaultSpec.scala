package busStops.fetcher

import org.scalacheck._
import org.scalacheck.Prop._
import org.scalacheck.Prop.AnyOperators

object BusStopsDataParserDefaultSpec extends Properties("BusStopsDataParserDefault") {

  private val twoToTenCharsWordGen: Gen[String] = for {
    length <- Gen.choose(2, 10)
    chars <- Gen.listOfN(length, Gen.alphaChar)
  } yield chars.mkString

  private val singleHeaderGen: Gen[String] = for {
    wordCount <- Gen.choose(1, 3)
    words <- Gen.listOfN(wordCount, twoToTenCharsWordGen)
  } yield words.reduce((lhs, rhs) => s"${lhs}_$rhs")

  private def charOrColumnSeparatorOrSpaceGen(columnSeparator: Char): Gen[Char] = Gen.frequency(
    (1, Gen.const(columnSeparator)),
    (1, Gen.const(' ')),
    (100, Gen.alphaNumChar)
  )

  private def zeroToTwentyCharsFieldGen(columnSeparator: Char): Gen[String] = for {
    length <- Gen.choose(0, 20)
    chars <- Gen.listOfN(length, charOrColumnSeparatorOrSpaceGen(columnSeparator))
  } yield chars.mkString

  private def rowGen(numberOfColumns: Int, columnSeparator: Char): Gen[Seq[String]] =
    Gen.listOfN(numberOfColumns, zeroToTwentyCharsFieldGen(columnSeparator))

  private def busStopsDataGen(columnSeparator: Char): Gen[BusStopsData] = for {
    numberOfColumns <- Gen.posNum[Int]
    numberOfRows <- Gen.posNum[Int]
    headers <- Gen.listOfN(numberOfColumns, singleHeaderGen)
    rows <- Gen.listOfN(numberOfRows, rowGen(numberOfColumns, columnSeparator))
  } yield BusStopsData(headers, rows)

  implicit val busStopsDataArbitrary: Arbitrary[BusStopsData] = Arbitrary(busStopsDataGen(','))

  property("parse/print round trip") = forAll { busStopsData: BusStopsData =>
    val columnSeparator = ','
    val print = BusStopsDataPrinterDefault().print(columnSeparator)
    val parse = BusStopsDataParserDefault().parse(columnSeparator)
    parse(print(busStopsData)) ?= Right(busStopsData)
  }
}
