package busStops.allBusStops

import org.scalacheck._
import org.scalacheck.Prop._
import org.scalacheck.Prop.AnyOperators

object StreetCodeLogicDefaultSpec extends Properties("StreetCodeLogicDefault"){

  final case class BusStopId(numericPrefix: String, alphaPart: String, numericSuffix: String) {
    val id: String = numericPrefix + alphaPart + numericSuffix
  }

  private def busStopIdGen(): Gen[BusStopId] = for {
    numericPrefix <- Gen.listOf(Gen.numChar)
    alphaPart <- Gen.alphaStr
    numericSuffix <- Gen.listOf(Gen.numChar)
  } yield BusStopId(numericPrefix.mkString, alphaPart, numericSuffix.mkString)

  implicit val busStopIdArbitrary: Arbitrary[BusStopId] = Arbitrary(busStopIdGen())

  property("street code from bus stop id") = forAll { busStopId: BusStopId =>
    val expectedStreetCode = if (busStopId.alphaPart.isEmpty)
      busStopId.numericPrefix + busStopId.numericSuffix
    else
      busStopId.alphaPart + busStopId.numericSuffix
    StreetCodeLogicDefault().streetCode(busStopId.id) ?= expectedStreetCode
  }
}
