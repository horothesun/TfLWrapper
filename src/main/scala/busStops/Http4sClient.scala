package busStops

import cats.effect.Resource
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.{Request, Response}
import zio.internal.PlatformLive
import zio.interop.catz._
import zio.{Runtime, Task}

import scala.concurrent.ExecutionContext
import scala.language.higherKinds

trait Http4sClient {
  def runRequest[T](req: Request[Task])(handler: Response[Task] => Task[T]): Task[T]
}

final case class Http4sClientDefault(executionContext: ExecutionContext) extends Http4sClient {

  implicit val rt: zio.Runtime[Any] = Runtime((), PlatformLive.fromExecutionContext(executionContext))
  private val http4sClient: Resource[Task, Client[Task]] = BlazeClientBuilder[Task](executionContext).resource

  override def runRequest[T](req: Request[Task])(handler: Response[Task] => Task[T]): Task[T] =
    http4sClient.use(_.fetch(req)(handler))
}
