package busStops

import busStops.allBusStops.GetAllBusStopsLogic
import service.Models.BusStop
import zio.IO

trait GetSingleBusStopLogic {
  def getSingleBusStop(stopId: String): IO[String, BusStop]
}

final case class GetSingleBusStopLogicDefault(getAllBusStopsLogic: GetAllBusStopsLogic) extends GetSingleBusStopLogic {

  def getSingleBusStop(stopId: String): IO[String, BusStop] = for {
    allBusStops <- getAllBusStopsLogic.getAllBusStops
    busStop     <- busStop(stopId, allBusStops)
  } yield busStop

  private def busStop(stopId: String, allBusStops: Seq[BusStop]): IO[String, BusStop] =
    IO.fromOption(allBusStops.find(_.id == stopId))
      .mapError { _ => s"""Can't find bus stop data for id = "$stopId"""" }
}
