package busStops.linesForBusStop

import busStops.Http4sClient
import io.circe.generic.auto._
import org.http4s._
import org.http4s.circe.jsonOf
import service.Models.LinesForBusStop
import zio.interop.catz._
import zio.{IO, Task}

trait GetLinesForBusStopLogic {
  def getLinesForBusStop(stopId: String): IO[String, LinesForBusStop]
}

final case class GetLinesForBusStopLogicDefault(http4sClient: Http4sClient) extends GetLinesForBusStopLogic {

  private final case class Arrival(id: String, lineId: String)
  private implicit val decoder: EntityDecoder[Task, Seq[Arrival]] = jsonOf[Task, Seq[Arrival]]

  override def getLinesForBusStop(stopId: String): IO[String, LinesForBusStop] =
    http4sClient
      .runRequest(request(stopId))(responseHandler(stopId))
      .mapError(_.getLocalizedMessage)

  private def request(stopId: String): Request[Task] =
    Request(Method.GET, uri(stopId), headers = Headers.of(Header("Accept", "application/json")))

  private def uri(stopId: String): Uri =
    Uri.unsafeFromString(s"https://api.tfl.gov.uk/StopPoint/$stopId/Arrivals")
      .withQueryParam("mode", "bus")

  private def responseHandler(stopId: String)(response: Response[Task]): Task[LinesForBusStop] =
    response.as[Seq[Arrival]]
      .map(lineIds)
      .map(LinesForBusStop(stopId, _))

  private def lineIds(arrivals: Seq[Arrival]): Seq[String] =
    arrivals.map(_.lineId)
      .toSet
      .toSeq
      .sorted(LineIdOrdering)
}
