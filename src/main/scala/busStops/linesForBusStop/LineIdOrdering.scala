package busStops.linesForBusStop

object LineIdOrdering extends Ordering[String] {
  override def compare(x: String, y: String): Int =
    (x.toIntOption, y.toIntOption) match {
      case (None,       Some(_))    => 1
      case (Some(_),    None)       => -1
      case (None,       None)       => Ordering[String].compare(x, y)
      case (Some(xInt), Some(yInt)) => Ordering[Int].compare(xInt, yInt)
    }
}
