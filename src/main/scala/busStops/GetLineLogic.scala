package busStops

import service.Models.Line
import org.http4s.{EntityDecoder, Header, Headers, Method, Request, Response, Uri}
import org.http4s.circe.jsonOf
import io.circe.generic.auto._
import zio.{IO, Task}
import zio.interop.catz._

trait GetLineLogic {
  def getLine(stopId: String, lineId: String): IO[String, Line]
}

final case class GetLineLogicDefault(http4sClient: Http4sClient) extends GetLineLogic {

  private final case class Arrival(id: String, lineId: String, timeToStation: Int)
  private implicit val decoder: EntityDecoder[Task, Seq[Arrival]] = jsonOf[Task, Seq[Arrival]]

  def getLine(stopId: String, lineId: String): IO[String, Line] =
    http4sClient
      .runRequest(request(stopId))(responseHandler(stopId, lineId))
      .mapError(_.getLocalizedMessage)

  private def request(stopId: String): Request[Task] =
    Request(Method.GET, uri(stopId), headers = Headers.of(Header("Accept", "application/json")))

  private def uri(stopId: String): Uri =
    Uri.unsafeFromString(s"https://api.tfl.gov.uk/StopPoint/$stopId/Arrivals")
      .withQueryParam("mode", "bus")

  private def responseHandler(stopId: String, lineId: String)(response: Response[Task]): Task[Line] =
    response.as[Seq[Arrival]]
      .map(timeToStations(stopId,lineId, _))
      .map(Line(stopId, lineId, _))

  private def timeToStations(stopId: String, lineId: String, arrivals: Seq[Arrival]): Seq[Int] =
    arrivals.filter(_.lineId == lineId)
      .map(_.timeToStation)
      .sorted
}
