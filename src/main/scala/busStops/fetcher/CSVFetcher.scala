package busStops.fetcher

import zio.Task

trait CSVFetcher {
  def fetchCSVLines: Task[Seq[String]]
}

final case class CSVFetcherDefault() extends CSVFetcher {

  private val csvFileName = "bus-stops-20190816-23-00.csv"

  override def fetchCSVLines: Task[Seq[String]] = Task.effect {
    scala.io.Source.fromResource(csvFileName).getLines.toSeq
  }
}
