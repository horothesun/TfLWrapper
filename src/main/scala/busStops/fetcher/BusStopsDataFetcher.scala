package busStops.fetcher

import zio.Task

final case class BusStopsData(headers: Seq[String], busStops: Seq[Seq[String]])

trait BusStopsDataFetcher {
  def busStops(columnSeparator: Char): Task[BusStopsData]
}

final case class BusStopsDataFetcherDefault(csvFetcher: CSVFetcher, parser: BusStopsDataParser) extends BusStopsDataFetcher {

  override def busStops(columnSeparator: Char): Task[BusStopsData] = for {
    csvLines <- csvFetcher.fetchCSVLines
    busStopsData <- Task.fromEither(
      parser.parse(columnSeparator)(csvLines)
    )
  } yield busStopsData
}
