package busStops.fetcher

import scala.util.matching.Regex
import scala.util.parsing.combinator._

sealed trait ParserError extends Error
case class Generic(message: String) extends ParserError

trait BusStopsDataParser {
  def parse(columnSeparator: Char): Seq[String] => Either[ParserError, BusStopsData]
}

final case class BusStopsDataParserDefault() extends BusStopsDataParser with RegexParsers {

  override def parse(columnSeparator: Char): Seq[String] => Either[ParserError, BusStopsData] =
    csvLines =>
      if (csvLines.isEmpty)
        Right(BusStopsData(Seq.empty, Seq.empty))
      else
        apply(columnSeparator)(csvLines.head).flatMap(
          // TODO: improve performance (fail fast)!!! 🔥🔥🔥
          headers => csvLines.tail.partitionMap(apply(columnSeparator)) match {
            case (error :: _, _) => Left(error)
            case (_, busStops) => Right(BusStopsData(headers, busStops))
          }
        )

  private sealed trait Token
  private case class COLUMN_SEPARATOR() extends Token
  private case class QUOTE() extends Token
  private case class WORD(value: String) extends Token
  private def apply(columnSeparator: Char)(input: String): Either[ParserError, Seq[String]] =
    parse(row(columnSeparator), input) match {
      case NoSuccess(message, _) => Left(Generic(message))
      case Success(result, _) => Right(result.map { case WORD(value) => value })
    }

  override def skipWhitespace = true
  override val whiteSpace: Regex = "[\t\r\f]+".r // no space

  private def row(columnSeparator: Char) =
    repsep(anyWord(columnSeparator), colSeparator(columnSeparator))
  private def anyWord(columnSeparator: Char) =
    unquotedNonEmptyWord | quotedEmptyWord | quotedNonEmptyWord(columnSeparator)
  private def quotedEmptyWord = quote ~ quote ^^^ WORD("")
  private def quotedNonEmptyWord(columnSeparator: Char) =
    quote ~ nonEmptyWord(columnSeparator) ~ quote ^^ { case _ ~ word ~ _ => word }
  private def nonEmptyWord(columnSeparator: Char) =
    s"""[0-9a-zA-Z#§±!@£%^&*()_+~?<>/':. $columnSeparator]+""".r ^^ { WORD }
  private def colSeparator(columnSeparator: Char) =
    columnSeparator.toString.r ^^^ COLUMN_SEPARATOR()
  private def quote = "\"".r ^^^ QUOTE()
  private def unquotedNonEmptyWord = """[0-9a-zA-Z#§±!@£$%^&*()_+~?\[\]<>/':. ]+""".r ^^ { WORD }
}
