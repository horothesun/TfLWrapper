package busStops.fetcher

trait BusStopsDataPrinter {
  def print(columnSeparator: Char): BusStopsData => Seq[String]
}

final case class BusStopsDataPrinterDefault() extends BusStopsDataPrinter {

  override def print(columnSeparator: Char): BusStopsData => Seq[String] =
    data => {
      val prettyHeaders: String = prettifyHeaders(columnSeparator)(data.headers)
      val prettyBusStops: Seq[String] = data.busStops.map(prettifyBusStopsRow(columnSeparator))
      Seq(prettyHeaders) ++ prettyBusStops
    }

  private def prettifyHeaders(columnSeparator: Char): Seq[String] => String =
    _.reduce(mergeFields(columnSeparator))

  private def prettifyBusStopsRow(columnSeparator: Char): Seq[String] => String =
    _.map(prettifyField(columnSeparator)).reduce(mergeFields(columnSeparator))

  private def prettifyField(columnSeparator: Char): String => String =
    field => if (field.isEmpty) "\"\"" else
      if (field.contains(columnSeparator) || field.contains(' ')) "\"" + field + "\"" else field

  private def mergeFields(columnSeparator: Char): (String, String) => String =
    (lhs, rhs) => s"$lhs$columnSeparator$rhs"
}
