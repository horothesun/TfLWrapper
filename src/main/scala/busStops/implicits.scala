package busStops

import busStops.allBusStops.{GetAllBusStopsLogic, GetAllBusStopsLogicDefault, StreetCodeLogicDefault}
import busStops.fetcher.{BusStopsDataFetcherDefault, BusStopsDataParserDefault, CSVFetcherDefault}
import zio.internal.PlatformLive.ExecutorUtil
import java.util.concurrent.{Executors, ThreadPoolExecutor}

import busStops.linesForBusStop.{GetLinesForBusStopLogic, GetLinesForBusStopLogicDefault}

object implicits {

  private val http4sClient: Http4sClient = {
    val threadPoolExecutor = Executors.newCachedThreadPool.asInstanceOf[ThreadPoolExecutor]
    val blockingContext = ExecutorUtil.fromThreadPoolExecutor(_ => Int.MaxValue)(threadPoolExecutor).asEC
    Http4sClientDefault(blockingContext)
  }

  implicit val getAllBusStopsLogic: GetAllBusStopsLogic = GetAllBusStopsLogicDefault(
    BusStopsDataFetcherDefault(CSVFetcherDefault(), BusStopsDataParserDefault()),
    StreetCodeLogicDefault()
  )
  implicit val getSingleBusStopLogic: GetSingleBusStopLogic = GetSingleBusStopLogicDefault(getAllBusStopsLogic)
  implicit val getLinesForBusStopLogic: GetLinesForBusStopLogic = GetLinesForBusStopLogicDefault(http4sClient)
  implicit val getLineLogic: GetLineLogic = GetLineLogicDefault(http4sClient)
}
