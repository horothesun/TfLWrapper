package busStops.allBusStops

trait StreetCodeLogic {
  def streetCode(busStopId: String): String
}

final case class StreetCodeLogicDefault() extends StreetCodeLogic {
  def streetCode(busStopId: String): String =
    busStopId.foldRight( (s"", false: Boolean, false: Boolean) ) { (c, partial) =>
      val (partialSuffix, isFullSuffixComplete, isDigitsSuffixComplete) = partial
      (isFullSuffixComplete, isDigitsSuffixComplete, c.isDigit) match {
        case (true,  _,     _)     => partial
        case (false, true,  true)  => (partialSuffix, true, isDigitsSuffixComplete)
        case (false, true,  false) |
             (false, false, true)  => (c + partialSuffix, isFullSuffixComplete, isDigitsSuffixComplete)
        case (false, false, false) => (c + partialSuffix, isFullSuffixComplete, true)
      }
    }._1
}
