package busStops.allBusStops

import busStops.fetcher.BusStopsDataFetcher
import service.Models.BusStop
import zio.IO

trait GetAllBusStopsLogic {
  def getAllBusStops: IO[String, Seq[BusStop]]
}

final case class GetAllBusStopsLogicDefault(fetcher: BusStopsDataFetcher,
                                            streetCodeLogic: StreetCodeLogic) extends GetAllBusStopsLogic {

  override def getAllBusStops: IO[String, Seq[BusStop]] =
    fetcher.busStops(',')
      .mapError(_.toString)
      .map { busStopsData => busStopsData.busStops.flatMap(optionBusStop) }

  private def optionBusStop(busStopData: Seq[String]): Option[BusStop] = for {
    busStopId <- busStopData.lift(2)
    stopName <- busStopData.lift(3)
  } yield BusStop(busStopId, nonEmptyStreetCode(busStopId), stopName)

  private def nonEmptyStreetCode(busStopId: String): String = {
    val result = streetCodeLogic.streetCode(busStopId)
    if (result.isEmpty) s"-" else result
  }
}
