package service

import service.docs.{JsonDocsProviderDefault, OpenApiDocsProviderDefault, SwaggerRoutesProviderDefault, YamlDocsProviderDefault}

object implicits {
  val serviceProvider: ServiceProvider = {
    val endpointsProvider = EndpointsProviderDefault()
    import busStops.implicits._
    val routesProvider = RoutesProviderDefault(endpointsProvider)
    val openApiDocsProvider = OpenApiDocsProviderDefault(endpointsProvider)
    val yamlDocsProvider = YamlDocsProviderDefault(openApiDocsProvider)
    val jsonDocsProvider = JsonDocsProviderDefault(openApiDocsProvider)
    val swaggerRoutesProvider = SwaggerRoutesProviderDefault(yamlDocsProvider, jsonDocsProvider)
    ServiceProviderDefault(routesProvider, swaggerRoutesProvider)
  }
}
