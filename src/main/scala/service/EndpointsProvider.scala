package service

import tapir._
import tapir.json.circe._
import io.circe.generic.auto._

import service.Models._

trait EndpointsProvider {
  val getAllBusStopsEndpoint: Endpoint[Unit, String, Seq[BusStop], Nothing]
  val getSingleBusStopEndpoint: Endpoint[String, String, BusStop, Nothing]
  val getLinesForBusStopEndpoint: Endpoint[String, String, LinesForBusStop, Nothing]
  val getLineEndpoint: Endpoint[(String, String), String, Line, Nothing]
}

final case class EndpointsProviderDefault() extends EndpointsProvider {

  private val version = endpoint.in("v1")
  private val busStopsEndpoint = version.in("bus-stops").errorOut(stringBody)
  private val singleBusStopEndpoint = busStopsEndpoint.in(path[String]("stopId"))
  private val linesForBusStopEndpoint = singleBusStopEndpoint.in("lines")
  private val lineEndpoint = linesForBusStopEndpoint.in(path[String]("lineId"))

  import EndpointsProviderDefault._

  val getAllBusStopsEndpoint: Endpoint[Unit, String, Seq[BusStop], Nothing] =
    busStopsEndpoint
      .get
      .description("Get all bus stops.")
      .out(jsonBody[Seq[BusStop]].example(allBusStopsOutExample))

  val getSingleBusStopEndpoint: Endpoint[String, String, BusStop, Nothing] =
    singleBusStopEndpoint
      .get
      .description("Get a single bus stop.")
      .out(jsonBody[BusStop].example(singleBusStopOutExample))

  val getLinesForBusStopEndpoint: Endpoint[String, String, LinesForBusStop, Nothing] =
    linesForBusStopEndpoint
      .get
      .description("Get all line ids for a given bus stop.")
      .out(jsonBody[LinesForBusStop].example(linesForBusStopOutExample))

  val getLineEndpoint: Endpoint[(String, String), String, Line, Nothing] =
    lineEndpoint
      .get
      .description("Get arrivals for a given line.")
      .out(jsonBody[Line].example(lineOutExample))
}

object EndpointsProviderDefault {
  val allBusStopsOutExample = Seq(
    BusStop("490012452SH", "SH", "ST MARY'S GROVE"),
    BusStop("490013537W", "W", "THE RISE")
  )
  val singleBusStopOutExample = BusStop("490001302S", "S", "UPPER HOLLOWAY STATION")
  val linesForBusStopOutExample = LinesForBusStop("490001302S", Seq("17", "263", "271", "43", "n41"))
  val lineOutExample = Line("490001302S", "271", Seq(162, 1173))
}
