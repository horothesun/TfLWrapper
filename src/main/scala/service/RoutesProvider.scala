package service

import tapir._
import tapir.server.http4s._
import org.http4s._
import zio.{IO, Task}
import zio.interop.catz._
import busStops._
import busStops.allBusStops.GetAllBusStopsLogic
import busStops.linesForBusStop.GetLinesForBusStopLogic

trait RoutesProvider {
  val getAllBusStopsRoute: HttpRoutes[Task]
  val getSingleBusStopRoute: HttpRoutes[Task]
  val getLinesForBusStopRoute: HttpRoutes[Task]
  val getLineRoute: HttpRoutes[Task]
}

final case class RoutesProviderDefault
  (endpointsProvider: EndpointsProvider)
  (implicit val getAllBusStopsLogic: GetAllBusStopsLogic,
   implicit val getSingleBusStopLogic: GetSingleBusStopLogic,
   implicit val getLinesForBusStopLogic: GetLinesForBusStopLogic,
   implicit val getLineLogic: GetLineLogic) extends RoutesProvider {

  implicit class ZioEndpoint[I, E, O](e: Endpoint[I, E, O, EntityBody[Task]]) {
    def toZioRoutes(logic: I => IO[E, O])(implicit serverOptions: Http4sServerOptions[Task]): HttpRoutes[Task] = {
      import tapir.server.http4s._
      e.toRoutes(logic(_).either)
    }
  }

  val getAllBusStopsRoute: HttpRoutes[Task] =
    endpointsProvider
      .getAllBusStopsEndpoint
      .toZioRoutes((_: Unit) => getAllBusStopsLogic.getAllBusStops)
  val getSingleBusStopRoute: HttpRoutes[Task] =
    endpointsProvider
      .getSingleBusStopEndpoint
      .toZioRoutes(getSingleBusStopLogic.getSingleBusStop)
  val getLinesForBusStopRoute: HttpRoutes[Task] =
    endpointsProvider
      .getLinesForBusStopEndpoint
      .toZioRoutes(getLinesForBusStopLogic.getLinesForBusStop)
  val getLineRoute: HttpRoutes[Task] =
    endpointsProvider
      .getLineEndpoint.toZioRoutes((getLineLogic.getLine _).tupled)
}