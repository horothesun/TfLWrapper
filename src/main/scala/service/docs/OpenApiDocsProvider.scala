package service.docs

import tapir.docs.openapi._
import tapir.openapi.OpenAPI

import service.EndpointsProvider

trait OpenApiDocsProvider {
  val documentation: OpenAPI
}

final case class OpenApiDocsProviderDefault(endpointsProvider: EndpointsProvider) extends OpenApiDocsProvider {
  import endpointsProvider._
  val documentation: OpenAPI =
    List(
      getAllBusStopsEndpoint,
      getSingleBusStopEndpoint,
      getLinesForBusStopEndpoint,
      getLineEndpoint
    )
    .toOpenAPI("TfL Wrapper", "0.1")
}