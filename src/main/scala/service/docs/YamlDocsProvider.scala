package service.docs

import tapir.openapi.circe.yaml._

trait YamlDocsProvider {
  val documentation: String
}

final case class YamlDocsProviderDefault(openApiDocsProvider: OpenApiDocsProvider) extends YamlDocsProvider {
  val documentation: String = openApiDocsProvider.documentation.toYaml
}
