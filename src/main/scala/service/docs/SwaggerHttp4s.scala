package service.docs

import cats.effect.{ContextShift, Sync}
import io.circe.Json
import org.http4s.dsl.Http4sDsl
import org.http4s.HttpRoutes

final class SwaggerHttp4s(yaml: String,
                          json: Json,
                          contextPath: String = "docs",
                          yamlName: String = "docs.yaml",
                          jsonName: String = "docs.json") {

  def routes[F[_]: ContextShift: Sync]: HttpRoutes[F] = {
    val dsl = Http4sDsl[F]
    import dsl._
    HttpRoutes.of[F] {
      case GET -> Root / `yamlName` => Ok(yaml)
      case GET -> Root / `jsonName` => Ok(json.toString())
    }
  }
}
