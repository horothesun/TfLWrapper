package service.docs

import io.circe.Json
import io.circe.syntax._
import tapir.openapi.circe._

trait JsonDocsProvider {
  val documentation: Json
}

final case class JsonDocsProviderDefault(openApiDocsProvider: OpenApiDocsProvider) extends JsonDocsProvider {
  val documentation: Json = openApiDocsProvider.documentation.asJson
}
