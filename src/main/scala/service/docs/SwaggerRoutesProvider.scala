package service.docs

import org.http4s._
import zio.Task
import zio.interop.catz._

trait SwaggerRoutesProvider {
  val routes: HttpRoutes[Task]
}

final case class SwaggerRoutesProviderDefault(yamlDocsProvider: YamlDocsProvider,
                                              jsonDocsProvider: JsonDocsProvider) extends SwaggerRoutesProvider {
  val routes: HttpRoutes[Task] = new SwaggerHttp4s(
    yamlDocsProvider.documentation,
    jsonDocsProvider.documentation
  ).routes[Task]
}
