package service

import cats.implicits._
import org.http4s._
import service.docs.SwaggerRoutesProvider
import zio.Task
import zio.interop.catz._

trait ServiceProvider {
  val service: HttpRoutes[Task]
}

final case class ServiceProviderDefault(routesProvider: RoutesProvider,
                                        swaggerRoutesProvider: SwaggerRoutesProvider) extends ServiceProvider {

  import routesProvider._
  val service: HttpRoutes[Task] =
    swaggerRoutesProvider.routes <+>
    getAllBusStopsRoute <+>
    getSingleBusStopRoute <+>
    getLinesForBusStopRoute <+>
    getLineRoute
}
