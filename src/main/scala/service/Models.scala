package service

object Models {
  final case class BusStop(id: String, streetCode: String, stopName: String)
  final case class LinesForBusStop(stopId: String, lineIds: Seq[String])
  final case class Line(stopId: String, lineId: String, arrivalsInSeconds: Seq[Int])
}
