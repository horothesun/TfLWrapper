import org.http4s.server.Router
import org.http4s.server.blaze._
import org.http4s.implicits._
import zio.{DefaultRuntime, Task}
import zio.console._
import zio.interop.catz._
import zio.interop.catz.implicits._

import scala.util.Properties.envOrNone

object Main extends scala.App {

  private val port = (envOrNone("PORT") orElse envOrNone("HTTP_PORT")).fold(8080)(_.toInt)

  private implicit val runtime: DefaultRuntime = new DefaultRuntime {}

  import service.implicits._
  private val httpApp = Router("/" -> serviceProvider.service).orNotFound
  private val server = BlazeServerBuilder[Task]
    .bindHttp(port, "0.0.0.0")
    .withHttpApp(httpApp)
    .serve
    .compile
    .drain
    .run

  private val program = for {
    _ <- putStrLn("ZIO + tapir + http4s 🙌")
    exit <- server
  } yield exit

  runtime.unsafeRun(program)
}
