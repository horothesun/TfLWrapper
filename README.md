# TfL Wrapper

[![pipeline status](https://gitlab.com/horothesun/TfLWrapper/badges/master/pipeline.svg)](https://gitlab.com/horothesun/TfLWrapper/commits/master) [![coverage report](https://gitlab.com/horothesun/TfLWrapper/badges/master/coverage.svg)](https://gitlab.com/horothesun/TfLWrapper/commits/master)

_Transport for London_ wrapper API to retrieve bus arrival times for a specific stop and line.

## Tech stack

- Implemented in _Scala 2.13_.
- Powered by [ZIO](https://github.com/zio/zio), [tapir](https://github.com/softwaremill/tapir) and [http4s](https://github.com/http4s/http4s).
- Tested with [ScalaCheck](https://github.com/typelevel/scalacheck).
- CI set up for automatic Heroku deployments.
- `TfLBusStopsCSVCleaner`: Swift playground used to download and clean the bus stops [CSV file](http://tfl.gov.uk/tfl/syndication/feeds/bus-stops.csv) provided by TfL.
Output: `~/Documents/Shared\ Playground\ Data/bus-stops-yyyyMMdd-HH-mm.csv`

## Endpoints

- `/v1/bus-stops`: all bus stops general info.
- `/v1/bus-stops/{stopId}`: single bus stop general info.
- `/v1/bus-stops/{stopId}/lines`: lines passing through a specific bus stop.
- `/v1/bus-stops/{stopId}/lines/{lineId}`: _times to station_ in seconds for a specific bus stop and line.

### OpenAPI documentation

Auto-generated OpenAPI documentation available at

- `/docs.yaml`
- `/docs.json`
