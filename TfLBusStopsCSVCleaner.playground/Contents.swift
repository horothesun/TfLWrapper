import PlaygroundSupport

precedencegroup LeftApplyPrecedence {
    associativity: left
    higherThan: AssignmentPrecedence
    lowerThan: TernaryPrecedence
}
precedencegroup FunctionCompositionPrecedence {
    associativity: right
    higherThan: LeftApplyPrecedence
}

infix operator |> : LeftApplyPrecedence
public func |> <A, B> (x: A, f: (A) -> B) -> B {
    return f(x)
}

infix operator >>> : FunctionCompositionPrecedence
func >>> <A, B, C> (f: @escaping (A) -> B, g: @escaping (B) -> C) -> (A) -> C {
    return { g(f($0)) }
}

extension ArraySlice {
    func asArray() -> [Element] { return Array(self) }
}

import Foundation

struct BusStopsData {
    let headers: [String]
    let busStops: [[String]]
}

protocol BusStopsPrettyPrinter {
    func pretty(
        busStopsData: BusStopsData,
        rowSeparator: Character,
        columnSeparator: Character) -> String
}

final class BusStopsPrettyPrinterDefault: BusStopsPrettyPrinter {

    private typealias `Self` = BusStopsPrettyPrinterDefault

    func pretty(
        busStopsData: BusStopsData,
        rowSeparator: Character,
        columnSeparator: Character) -> String {

        let prettyHeaders = busStopsData.headers
            .map(Self.prettyValue(with: columnSeparator))
            .joined(separator: "\(columnSeparator)")
        let prettyBusStops = busStopsData.busStops
            .map { busStops -> String in
                busStops
                    .map(Self.prettyValue(with: columnSeparator))
                    .joined(separator: "\(columnSeparator)")
            }
        let prettyRows = [prettyHeaders] + prettyBusStops
        return prettyRows.joined(separator: "\(rowSeparator)")
    }

    private static func prettyValue(
        with columnSeparator: Character) -> (_ value: String) -> String {

        return { value in
            if value.isEmpty {
                return "\"\""
            } else {
                return value.contains(columnSeparator) ? "\"\(value)\"" : value
            }
        }
    }
}


protocol BusStopsFetcherSync {
    func busStops(
        urlString: String,
        rowSeparator: Character,
        columnSeparator: Character) throws -> BusStopsData
}

struct BusStopsFetcherSyncDefault: BusStopsFetcherSync {

    private typealias `Self` = BusStopsFetcherSyncDefault

    struct FetcherError: Error { }

    private static let naptanAtcoHeader = "Naptan_Atco"

    func busStops(
        urlString: String,
        rowSeparator: Character,
        columnSeparator: Character) throws -> BusStopsData {

        guard let url = URL(string: urlString),
            let data = try? Data(contentsOf: url),
            let csvString = String(data: data, encoding: .utf8) else { throw FetcherError() }

        return try busStops(
            from: csvString |> Self.csvStringWithoutRowSeparatorDoubleQuotes(with: rowSeparator),
            rowSeparator: rowSeparator,
            columnSeparator: columnSeparator
        )
    }

    // fixing a problem of ~130 rows split in two rows
    private static func csvStringWithoutRowSeparatorDoubleQuotes(
        with rowSeparator: Character) -> (String) -> String {

        return { csvString in
            csvString.replacingOccurrences(of: "\(rowSeparator)\"", with: "\"")
        }
    }

    private func busStops(
        from csvString: String,
        rowSeparator: Character,
        columnSeparator: Character) throws -> BusStopsData {

        let rowStrings: [String] = csvString
            .split(separator: rowSeparator)
            .map(String.init(_:))
        guard let headerRowString = rowStrings.first else { throw FetcherError() }
        let numberOfColumns = 1 + numberOf(columnSeparator, in: headerRowString)
        var rowStringSet: Set<String> = .init(minimumCapacity: rowStrings.count)
        rowStrings.forEach { rowString in
            if rowString.count > 2 * (numberOfColumns - 1),
                !rowString.lowercased().contains(Self.naptanAtcoHeader.lowercased()) {
                rowStringSet.insert(rowString)
            }
        }
        let headers = headerRowString
            .split(separator: columnSeparator)
            .map(String.init(_:))
        let busStops = rowStringSet
            .map(
                Self.busStopFromRowString(with: columnSeparator)
                    >>> Self.mergeOriginallySplitBusStop(with: numberOfColumns, columnSeparator)
                    >>> {
                        $0.map(
                            Self.removeDoubleQuotesFrom(busStopField:)
                                >>> Self.trimWhiteSpacesFrom(busStopField:)
                        )
                    }
            )
        return .init(headers: headers, busStops: busStops)
    }

    private static func busStopFromRowString(
        with columnSeparator: Character) -> (String) -> [String] {

        return { rowString in
            rowString.split(separator: columnSeparator).map(String.init(_:))
        }
    }

    private static func mergeOriginallySplitBusStop(
        with numberOfColumns: UInt,
        _ columnSeparator: Character) -> (_ busStop: [String]) -> [String] {

        return { busStop in
            let numberOfExtraStrings = busStop.count - Int(numberOfColumns)
            switch numberOfExtraStrings {
            case ..<1:
                return busStop
            default:
                let firstIncorrectIndex = 3
                let firstCorrectIndexAfterExtraStrings = firstIncorrectIndex + numberOfExtraStrings
                return busStop[0..<firstIncorrectIndex].asArray()
                    + [
                        busStop[firstIncorrectIndex...firstCorrectIndexAfterExtraStrings].asArray()
                            .joined(separator: "\(columnSeparator)")
                    ]
                    + busStop[(1 + firstCorrectIndexAfterExtraStrings)...].asArray()
            }
        }
    }

    private static func removeDoubleQuotesFrom(busStopField: String) -> String {
        return busStopField.replacingOccurrences(of: "\"", with: "")
    }

    private static func trimWhiteSpacesFrom(busStopField: String) -> String {
        return busStopField.trimmingCharacters(in: .whitespaces)
    }

    private func numberOf(_ character: Character, in string: String) -> UInt {
        var charCount: UInt = 0
        string.forEach { c in charCount += c == character ? 1 : 0 }
        return charCount
    }
}

func fileNameFrom(currentDate: Date) -> String {
    let dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "YYYYMMdd-HH-mm"
        return df
    }()
    let currentDateString = dateFormatter.string(from: currentDate)
    return "bus-stops-\(currentDateString).csv"
}

// https://medium.com/@vhart/read-write-and-delete-file-handling-from-xcode-playground-abf57e445b4
// saves file in ~/Documents/Shared\ Playground\ Data/
func write(_ busStopsString: String, to fileName: String) throws {
    let fileUrl = playgroundSharedDataDirectory.appendingPathComponent(fileName)
    do { try busStopsString.write(to: fileUrl, atomically: true, encoding: .utf8) }
    catch { print(error) }
}

func main() throws {
    let urlString = "http://tfl.gov.uk/tfl/syndication/feeds/bus-stops.csv"
    let rowSeparator: Character = "\r\n"
    let columnSeparator: Character = ","

    let busStopsFetcher: BusStopsFetcherSync = BusStopsFetcherSyncDefault()
    let busStopsData = try busStopsFetcher
        .busStops(
            urlString: urlString,
            rowSeparator: rowSeparator,
            columnSeparator: columnSeparator
        )

    print(busStopsData.headers)
    let numberOfRows = busStopsData.busStops.count
    let numberOfColumns = busStopsData.headers.count
    print("CSV \(numberOfRows) rows \(numberOfColumns) columns")

    print("👀 Bus Stops data check...")
    busStopsData.busStops.forEach { busStopData in
        if busStopData.count != numberOfColumns { print("🚨 \(busStopData)") }
    }
    print("Check completed 👀")

    let prettyPrinter: BusStopsPrettyPrinter = BusStopsPrettyPrinterDefault()
    let prettyBusStops = prettyPrinter.pretty(
        busStopsData: busStopsData,
        rowSeparator: rowSeparator,
        columnSeparator: columnSeparator
    )

    let fileName = fileNameFrom(currentDate: .init())
    print("💾 Writing to file '~/Documents/Shared\\ Playground\\ Data/\(fileName)'...")
    try write(prettyBusStops, to: fileName)
    print("Writing completed 💾")

    PlaygroundPage.current.finishExecution()
}



try main()
