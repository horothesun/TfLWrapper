# Multi-stage build with sbt-native-packager

FROM openjdk:11.0.3 as builder

# Install sbt
ENV SBT_VERSION 1.2.8
RUN \
  curl -L -o sbt-$SBT_VERSION.deb https://dl.bintray.com/sbt/debian/sbt-$SBT_VERSION.deb && \
  dpkg -i sbt-$SBT_VERSION.deb && \
  apt-get update && \
  sbt sbtVersion

WORKDIR /build

# Cache dependencies first
COPY project project
COPY build.sbt .
RUN sbt update

# Then build
COPY . .
ENV SBT_OPTS -Xmx256M
RUN sbt stage


FROM openjdk:11.0.3-jre-slim

ENV HTTP_PORT 8080
EXPOSE $HTTP_PORT

WORKDIR /app
COPY --from=builder /build/target/universal/stage/. .
RUN mv bin/$(ls bin | grep -v .bat) bin/start

CMD ["./bin/start"]
